import 'package:flutter/material.dart';
import 'package:flutter_application_module_3/register.dart';
import 'package:flutter_application_module_3/login.dart';
import 'package:flutter_application_module_3/dashboard.dart';
import 'package:flutter_application_module_3/features.dart';
import 'package:flutter_application_module_3/useredit.dart';



void main() {
  runApp(MaterialApp(
    initialRoute: '/',
    routes: {
      '/': (context) => const Home(),
      '/register': (context) => const Register(),
      '/login':(context) => const Login(),
      '/dashboard': (context) => const Dashboard(),
      '/useredit':(context) => const Edit(),
      '/features':(context) => const Features()
    },
  ));
}

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Home'),
        backgroundColor: Colors.blueGrey,
      ),
      body: Center(
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 2.0),
          child: GridView.count(
              crossAxisCount: 1,
              padding: const EdgeInsets.all(3.0),
              children: <Widget>[
                makeDashboardItem("Register", Icons.note),
              ]),
        ),
      ),
    );
  }

  Card makeDashboardItem(String title, IconData icon) {
    return Card(
      elevation: 1.0,
      margin: const EdgeInsets.all(8.0),
      child: Container(
        decoration: const BoxDecoration(color: Colors.blueGrey),
        child: InkWell(
          onTap: () {
            Navigator.pushNamed(context, '/register');
          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            verticalDirection: VerticalDirection.down,
            children: <Widget>[
              const SizedBox(height: 50.0),
              Center(
                child: Icon(
                  icon,
                  size: 40.0,
                  color: Colors.black,
                ),
              ),
              const SizedBox(height: 20.0),
              Center(
                child: Text(title,
                    style:
                        const TextStyle(fontSize: 18.0, color: Colors.black)),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
