import 'package:flutter/material.dart';
import 'package:flutter_application_module_3/dashboard.dart';
import 'package:flutter_application_module_3/useredit.dart';

void main() {
  runApp(MaterialApp(
    initialRoute: 'dashboard',
    routes: {
      '/dashboard': (context) => const Dashboard(),
      'useredit': (context) => const Edit()
    },
  ));
}

class Features extends StatefulWidget {
  const Features({Key? key}) : super(key: key);

  @override
  State<Features> createState() => _FeaturesState();
}

class _FeaturesState extends State<Features> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Features'),
        backgroundColor: Colors.blueGrey,
      ),
       body: Center(
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 5.0, horizontal: .5),
          child: GridView.count(
              crossAxisCount: 3,
              padding: const EdgeInsets.all(3.0),
              children: <Widget>[
                makeDashboardItem("Register", Icons.book),
                makeDashboardItem("Login", Icons.door_back_door),
                makeDashboardItem("Home", Icons.house),
              ]),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, '/useredit');
        },
        child: const Icon(
          Icons.person
        ), 
        ),
    );
  }


  Card makeDashboardItem(String title, IconData icon) {
    return Card(
      elevation: 1.0,
      margin: const EdgeInsets.all(8.0),
      child: Container(
        decoration: const BoxDecoration(color: Colors.blueGrey),
        child: InkWell(
          onTap: () {
            Navigator.pushNamed(context, '/');
          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            verticalDirection: VerticalDirection.down,
            children: <Widget>[
              const SizedBox(height: 50.0),
              Center(
                child: Icon(
                  icon,
                  size: 40.0,
                  color: Colors.black,
                ),
              ),
              const SizedBox(height: 20.0),
              Center(
                child: Text(title,
                    style:
                        const TextStyle(fontSize: 18.0, color: Colors.black)),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

 