import 'package:flutter/material.dart';
import 'package:flutter_application_module_3/home.dart';
import 'package:flutter_application_module_3/login.dart';
import 'package:flutter_application_module_3/dashboard.dart';


void main() {
  runApp(MaterialApp(
    initialRoute: '/dashboard',
    routes: {
      '/':(context) => const Home(),
      '/useredit': (context) => const Edit(),
      '/login': (context) => const Login(),
      '/dashboard':(context) => const Dashboard()
    },
  ));
}

class Edit extends StatefulWidget {
  const Edit({Key? key}) : super(key: key);

  @override
  State<Edit> createState() => _EditState();
}

class _EditState extends State<Edit> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('User Profile Edit'),
      ),
      body: Container(
        margin: const EdgeInsets.all(15.0),
        padding: const EdgeInsets.all(15.0),
        child: Column(
          children: <Widget>[
            const Padding(
              padding: EdgeInsets.all(15.0),
              child: TextField(
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Edit Name',
                    hintText: 'Edit your Name'),
              ),
            ),
            const Padding(
              padding: EdgeInsets.all(15.0),
              child: TextField(
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Last Name',
                    hintText: 'Edit Last Name'),
              ),
            ),
            const Padding(
              padding: EdgeInsets.all(15.0),
              child: TextField(
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Edit Password',
                    hintText: 'Change Password'),
              ),
            ),
            ElevatedButton(
                child: const Text('Save'),
                onPressed: () {
                  Navigator.pushNamed(context, '/dashboard');
                })
          ],
        ),
      ),
    );
  }
}
