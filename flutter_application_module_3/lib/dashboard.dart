import 'package:flutter/material.dart';
import 'package:flutter_application_module_3/features.dart';
import 'package:flutter_application_module_3/home.dart';
import 'package:flutter_application_module_3/register.dart';
import 'package:flutter_application_module_3/login.dart';

void main() {
  runApp(MaterialApp(
    initialRoute: '/login',
    routes: {
      '/': (context) => const Home(),
      '/dashboard': (context) => const Dashboard(),
      '/register': (context) => const Register(),
      '/login': (context) => const Login(),
      '/features':(context) => const Features()

    },
  ));
}

class Dashboard extends StatefulWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  State<Dashboard> createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Dashboard'),
        backgroundColor: Colors.blueGrey,
      ),
      body: Center(
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 2.0),
          child: GridView.count(
              crossAxisCount: 1,
              padding: const EdgeInsets.all(3.0),
              children: <Widget>[
                // makeDashboardItem("User Profile Edit", Icons.person),
                makeDashboardItem("Features", Icons.computer),
              ]),
        ),
      ),
    );
  }

  Card makeDashboardItem(String title, IconData icon) {
    return Card(
      elevation: 1.0,
      margin: const EdgeInsets.all(8.0),
      child: Container(
        decoration: const BoxDecoration(color: Colors.blueGrey),
        child: InkWell(
          onTap: () {
            Navigator.pushNamed(context, '/features');
          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            verticalDirection: VerticalDirection.down,
            children: <Widget>[
              const SizedBox(height: 50.0),
              Center(
                child: Icon(
                  icon,
                  size: 40.0,
                  color: Colors.black,
                ),
              ),
              const SizedBox(height: 20.0),
              Center(
                child: Text(title,
                    style:
                        const TextStyle(fontSize: 18.0, color: Colors.black)),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
