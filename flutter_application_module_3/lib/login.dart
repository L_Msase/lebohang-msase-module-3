import 'package:flutter/material.dart';
import 'package:flutter_application_module_3/dashboard.dart';
import 'package:flutter_application_module_3/home.dart';
import 'package:flutter_application_module_3/features.dart';
import 'package:flutter_application_module_3/register.dart';


void main() {
  runApp(MaterialApp(
    initialRoute: '/register',
    routes: {
      '/':(context) => const Home(),
      '/register': (context) => const Register(),
      '/login': (context) => const Login(),
      '/dashboard': (context) => const Dashboard(),
      '/features':(context) => const Features()
    },
  ));
}

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Login'),
      ),
      body: Container(
        margin: const EdgeInsets.all(15.0),
        padding: const EdgeInsets.all(15.0),
        child: Column(
          children: <Widget>[
            const Padding(
              padding: EdgeInsets.all(15.0),
              child: TextField(
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'username',
                    hintText: 'First & Last Name'),
              ),
            ),
            const Padding(
              padding: EdgeInsets.all(15.0),
              child: TextField(
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Password',
                    hintText: 'Enter Password'),
              ),
            ),
            ElevatedButton(
                child: const Text('Login'),
                onPressed: () {
                  Navigator.pushNamed(context, '/dashboard');
                })
          ],
        ),
      ),
    );
  }
}
