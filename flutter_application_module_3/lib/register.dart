import 'package:flutter/material.dart';
import 'package:flutter_application_module_3/login.dart';
import 'package:flutter_application_module_3/dashboard.dart';
import 'package:flutter_application_module_3/features.dart';



void main() {
  runApp(MaterialApp(
    initialRoute: '/register',
    routes: {
      '/register': (context) => const Register(),
      '/login': (context) => const Login(),
      '/dashboard':(context) => const Dashboard(),
      '/features':(context) => const Features()

    },
  ));
}

class Register extends StatefulWidget {
  const Register({Key? key}) : super(key: key);

  @override
  State<Register> createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Register'),
      ),
      body: Container(
        margin: const EdgeInsets.all(15.0),
        padding: const EdgeInsets.all(15.0),
        child: Column(
          children: <Widget>[
            const Padding(
              padding: EdgeInsets.all(15.0),
              child: TextField(
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'First Name',
                    hintText: 'Enter your Name'),
              ),
            ),
            const Padding(
              padding: EdgeInsets.all(15.0),
              child: TextField(
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Last Name',
                    hintText: 'Enter your Name'),
              ),
            ),
            const Padding(
              padding: EdgeInsets.all(15.0),
              child: TextField(
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Password',
                    hintText: 'Enter Password'),
              ),
            ),
            ElevatedButton(
                child: const Text('Register'),
                onPressed: () {
                  Navigator.pushNamed(context, '/login');
                })
          ],
        ),
      ),
    );
  }
}
